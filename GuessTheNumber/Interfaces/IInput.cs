﻿namespace GuessTheNumber.Interfaces
{
    public interface IInput
    {
        public string ReadLine();
    }
}

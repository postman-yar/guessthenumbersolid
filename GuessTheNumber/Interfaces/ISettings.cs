﻿namespace GuessTheNumber.Interfaces
{
    public interface ISettings
    {
        public int MinValue { get; }
        public int MaxValue { get; }
        public int NumberOfAttempts { get; }

    }
}

﻿namespace GuessTheNumber.Interfaces
{
    public interface IOutput
    {
        public void Print(string str);
    }
}

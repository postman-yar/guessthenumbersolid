﻿namespace GuessTheNumber.Interfaces
{
    public interface IRandom
    {
        public int Next(int from, int to);
    }
}

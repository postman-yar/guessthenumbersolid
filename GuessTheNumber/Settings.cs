﻿using GuessTheNumber.Interfaces;

namespace GuessTheNumber
{
    //SRP – принцип единой ответственности, тут можно было бы сразу возвращать рандомное число,
    //но мы вынесли этот функционал в отдельный класс HiddenNumberHelper
    public class Settings : ISettings
    {
        public int MinValue { get; }
        public int MaxValue { get; }
        public int NumberOfAttempts { get; }

        public Settings(int minValue, int maxValue, int numberOfAttempts)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfAttempts = numberOfAttempts;
        }
    }
}

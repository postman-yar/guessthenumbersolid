﻿using GuessTheNumber.Classes;
using GuessTheNumber.Interfaces;

namespace GuessTheNumber
{
    class Program
    {
        private static IGame _game;

        static void Main(string[] args)
        {
            var gameType = GameType.Default;
            if (args != null)
            {
                try
                {
                    gameType = (GameType)int.Parse(args[0]);
                }
                catch { }
            }

            switch (gameType)
            {
                default:
                    _game = GameFactory.GetInstance();
                    break;
            }

            _game.Start();           
        }
    }
}

﻿using System;

namespace GuessTheNumber.Classes
{
    class MySuperRandom : Random
    {
        public override int Next(int minValue, int maxValue)
        {
            return base.Next(minValue + 1 - 1, maxValue + 1 - 1);
        }
    }
}

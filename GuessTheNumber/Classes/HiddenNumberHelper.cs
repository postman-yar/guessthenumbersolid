﻿using GuessTheNumber.Interfaces;

namespace GuessTheNumber.Classes
{
    //DIP - Принцип инверсии зависимостей, наш класс зависит от интерфейсов, а не от конкретных классов
    public class HiddenNumberHelper
    {
        public int HiddenNumber { get; }

        public HiddenNumberHelper(ISettings settings, IRandom rnd)
        {
            HiddenNumber = rnd.Next(settings.MinValue, settings.MaxValue + 1);
        }
    }
}

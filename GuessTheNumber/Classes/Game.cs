﻿using GuessTheNumber.Interfaces;

namespace GuessTheNumber.Classes
{
    public class Game : IGame
    {
        private readonly ISettings _settings;
        private readonly IO _io;
        private int _hiddenNumber;
        private int _attemptNumber;

        public Game(ISettings settings, IO io)
        {
            _settings = settings;
            _io = io;
        }
        public void Start()
        {
            _hiddenNumber = new HiddenNumberHelper(_settings, new MyRandom()).HiddenNumber;

            _attemptNumber = 0;
            _io.Print("Давай поиграем!");
            _io.Print(AsciiPic.letsPlay);
            _io.Print($"Загаданное число: {_hiddenNumber}");
            _io.Print(
                $"Угадайте число от {_settings.MinValue} до {_settings.MaxValue}, число попыток {_settings.NumberOfAttempts}");

            while (_attemptNumber++ < _settings.NumberOfAttempts)
            {
                var input = _io.ReadLine();
                var result = int.TryParse(input, out var number);
                if (result)
                {
                    if (ProcessANumber(number))
                        break;
                }
                else
                {
                    _io.Print(
                        $":-(, попробуйте еще раз, Вы ввели неправильное значение, осталось {_settings.NumberOfAttempts - _attemptNumber} попыток");
                }
            }
        }

        bool ProcessANumber(int number)
        {
            if (number == _hiddenNumber)
            {
                _io.Print(AsciiPic.youWin2);
                _io.Print($"Поздравляю, Вы угадали число с {_attemptNumber} попытки");
                return true;
            }
            if (number > _settings.MaxValue || number < _settings.MinValue)
            {
                _io.Print(
                    $":-(, попробуйте еще раз, Ваше число вне диапазона, осталось {_settings.NumberOfAttempts - _attemptNumber} попыток");
                return false;
            }

            _io.Print(
                $":-(, попробуйте еще раз, Ваше число {(number > _hiddenNumber ? "больше" : "меньше")} загаданного, осталось {_settings.NumberOfAttempts - _attemptNumber} попыток");
            return false;
        }
    }
}

﻿using GuessTheNumber.Interfaces;

namespace GuessTheNumber.Classes
{
    public static class GameFactory
    {
        private static readonly Game Game;

        static GameFactory()
        {
            var settings = new Settings(0, 10, 50);
            var io = new IOConsole128();
            Game = new Game(settings, io);
        }

        public static Game GetInstance()
        {
            return Game;
        }

    }
}

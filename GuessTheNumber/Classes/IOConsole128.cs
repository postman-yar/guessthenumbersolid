﻿using System;

namespace GuessTheNumber.Interfaces
{
    public class IOConsole128 : IO
    {
        public override void Print(string str)
        {
            Console.WriteLine(str);
        }

        public override string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}

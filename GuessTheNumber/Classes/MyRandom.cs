﻿using GuessTheNumber.Interfaces;
using System;

namespace GuessTheNumber.Classes
{
    // LCP - Принцип подстановки Лисков, вместо класса MySuperRrandom мы можем использовать класс Rrandom, от которого унаследован класс MySuperRrandom
    public class MyRandom : IRandom
    {
        private readonly MySuperRandom _rnd = new();
        public int Next(int from, int to)
        {
            return _rnd.Next(from, to);
        }
    }
}

﻿using System;

namespace GuessTheNumber.Interfaces
{
    //OCP - принцип открытости/закрытости
    //Если захотим выводить не в консоль, то создадим класс, унаследуем его от IOutput и переопределим функцию Print

    //ISP - Принцип разделения интерфейсов
    public class IO : IOutput, IInput
    {
        public virtual void Print(string str)
        {
            Console.WriteLine(str);
        }

        public virtual string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
